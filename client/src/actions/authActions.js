import axios from 'axios';
import {GET_ERRORS, SET_CURRENT_USER} from './types.js';
import setAuthToken from '../utils/setAuthToken'
import jwt_decode from 'jwt-decode'


export const registerUser = (userData, history) => dispatch => {
  axios
    .post('/api/users/register', userData)
    .then(res => history.push('/login'))
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    });
};


//Login - Get user token
export const loginUser = userData => dispatch => {
  axios.post('/api/users/login', userData)
    .then(res => {
      //Save to localStorage
      const {token} = res.data;
      //set token to ls
      localStorage.setItem('jwtToken', token);
      //setToken to auth header
      setAuthToken(token);
      //decode to get user data
      const decoded = jwt_decode(token);
      dispatch(setCurrentUser(decoded))
    })
    .catch(err =>
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    }))
}


export const setCurrentUser = (decoded) => {
  return ({
    type: SET_CURRENT_USER,
    payload: decoded
  })
}

//log out
export const logoutUser = () => dispatch => {
  //Remove token
  localStorage.removeItem('jwtToken');
  setAuthToken(false);
  dispatch(setCurrentUser({}));
}