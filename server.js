const express = require('express');
const mongoose = require('mongoose')

const bodyParser = require('body-parser');
const users = require('./routes/api/users');
const profile = require('./routes/api/profile');
const posts = require('./routes/api/posts');
const passport = require('passport');


const app  = express();
//body Parser middleware

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json())

//passport middleware
app.use(passport.initialize());

//passport config
require('./config/passport')(passport);

//use routes
app.use('/api/users', users);
app.use('/api/profile', profile);
app.use('/api/posts', posts);

//DB Config
const db = require('./config/keys').mongoURI;

//Connect to mLAb (mongoDB)
mongoose
  .connect(db)
  .then(()=> console.log('mongoDB connected'))
  .catch(err=> console.log(err));
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));
